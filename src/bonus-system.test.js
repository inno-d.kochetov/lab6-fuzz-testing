import {calculateBonuses} from "./bonus-system.js";

const assert = require("assert");

function test_cases(m, p, cases) {
    for (let i = 0; i < cases.length; i++) {
        test('test ' + i, (done) => {
            assert.equal(calculateBonuses(p, cases[i][0]), m * cases[i][1]);
            done();
        });
    }
}

describe('Standard', () => {
    const m = 0.05;
    const p = 'Standard';
    const cases = [[5000, 1], [10000, 1.5], [25000, 1.5], [50000, 2], [75000, 2], [100000, 2.5], [200000, 2.5]];
    test_cases(m, p, cases)
});


describe('Premium', () => {
    const m = 0.1;
    const p = 'Premium';
    const cases = [[5000, 1], [10000, 1.5], [25000, 1.5], [50000, 2], [75000, 2], [100000, 2.5], [200000, 2.5]];
    test_cases(m, p, cases)
});


describe('Diamond', () => {
    const m = 0.2;
    const p = 'Diamond';
    const cases = [[5000, 1], [10000, 1.5], [25000, 1.5], [50000, 2], [75000, 2], [100000, 2.5], [200000, 2.5]];
    test_cases(m, p, cases)
});


describe('Others', () => {
    const m = 0;
    const p = 'wrong';
    const cases = [[5000, 1], [10000, 1.5], [25000, 1.5], [50000, 2], [75000, 2], [100000, 2.5], [200000, 2.5]];
    test_cases(m, p, cases)
});
